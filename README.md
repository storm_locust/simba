# Simba 介紹 #

這是一個支援自動產生 Django Project 以及加入部分修正的支援系統

### 怎麼安裝? ###

#### 需求 ： python3.7, Django 3.2.6

1. 克隆到自己目錄下
2. cd simba
3. pipenv install  【安裝虛擬環境，如果沒有請先安裝pipenv】


### 怎麼使用？
------

```shell
python main.py -h
```

```html
usage: main.py [-h] [-p PATH] [-c] [--delproject DELPROJECT DELPROJECT]
               new name [name ...]

Calamus & Glider's Django auto generate tool

positional arguments:
  new                   create Django project's base structure
  name                  Django project name

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  Django project path
  -c, --channels        Add channels settings
```

------

#### 建立專案範例
```shell
python main.py new 'project name' -c -p 'project folder path'
```

#### 建立APP範例
```shell
python manage.py newapp 'app name' or
python ../manage.py newapp 'app name'
```