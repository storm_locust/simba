import os
import subprocess
from argparse import ArgumentParser
from subprocess import PIPE

from PathLib.Settings.Patch import PathSettings
from PathLib.Url.urls_update import UrlsUpdate


class Simba:
    def __init__(self):
        self.django_cmd = 'django-admin'
        self.parser = ArgumentParser()

    def new_project(self, *args):
        current_path = os.getcwd()
        project_name = getattr(*args, "name")[0]
        assert project_name, "NOT GET PROJECT... (-n `project name`)"
        project_path = getattr(*args, "path")
        project_channels = getattr(*args, "channels")
        if project_path is None:
            project_path = "."
        else:
            if not os.path.exists(project_path):
                os.makedirs(project_path, exist_ok=True)
            os.chdir(project_path)
        self.new_pt = '{} startproject {}'.format(self.django_cmd, project_name)

        pn = subprocess.Popen(self.new_pt,
                              shell=True,
                              stdout=PIPE,
                              stderr=PIPE,
                              stdin=PIPE)
        while True:
            line = pn.stdout.readline()
            err_line = pn.stderr.readline()
            if err_line:
                print("NEW PROJECT ERR >>> ", err_line.decode())
                break
            if not line:
                break
        assert os.path.exists(project_name), "CREATE PROJECT ERR!...."
        os.chdir(current_path)
        self.ul = UrlsUpdate()
        file_path = os.path.join(project_path, project_name, project_name, "urls.py")
        self.ul.file_operation(file_path)

        self.su = PathSettings()
        file_path = os.path.join(project_path, project_name, project_name, "settings.py")
        self.su.file_operation(file_path)
        self.su.UpdateSettings(channels=project_channels)
        self.su.auto_building_folder()
        self.su.copy_app()


parser = ArgumentParser(description="Calamus & Glider's Django auto generate tool")
parser.add_argument("new", nargs=1, help="create Django project's base structure")
parser.add_argument(type=str, nargs='+', help="Django project name", dest="name", default="mytest")
parser.add_argument("-p", "--path", help="Django project path", dest="path", required=False)
parser.add_argument("-c", "--channels", help="Add channels settings", action='store_true', dest="channels",
                    required=False)

args = parser.parse_args()
cgd = Simba()

if args.new:
    if args.channels is None:
        print("《《《 Generate Basic Django Project 「{}」》》》".format(args.name))
    else:
        print("《《《 Generate Channels Django Project 「{}」》》》".format(args.name))
    cgd.new_project(args)
