import os
from unittest import TestCase

from PathLib.Settings.Patch import PathSettings


class TestSettingsUpdate(TestCase):
    cPath = os.getcwd()
    def setUp(self) -> None:
        ProjectName = "mytest1"
        ProjectSettingPath = os.path.join(self.cPath, ProjectName, ProjectName, "settings.py")
        os.system("django-admin startproject {}".format(ProjectName))
        self.su = PathSettings()
        self.su.file_operation(ProjectSettingPath)

    def test_insert_data(self):
        self.su.UpdateSettings()
        self.su.auto_building_folder()
        self.su.copy_app()

    def tearDown(self) -> None:
        del self.su
