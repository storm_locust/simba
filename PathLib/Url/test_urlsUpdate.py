import os
from unittest import TestCase

from PathLib.Url.urls_update import UrlsUpdate


class TestUrlsUpdate(TestCase):
    cPath = os.getcwd()

    def setUp(self) -> None:
        ProjectName = "mytest1"
        ProjectSettingPath = os.path.join(self.cPath, ProjectName, ProjectName, "urls.py")
        os.system("django-admin startproject {}".format(ProjectName))
        self.su = UrlsUpdate()
        self.su.file_operation(ProjectSettingPath)

    def test_insert_data(self):
        self.su.update_urls()

    def tearDown(self) -> None:
        del self.su
